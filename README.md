CD with gitlab
========================
Goal is to deploy to master every time we push code to repo


References
========================
https://blog.jacobtan.co/deploying-from-gitlab-to-digitalocean/

https://gitlab.com/help/ci/quick_start/README


Steps
========================
- add variables in gitlab.com
	- go to  repository
	- Settings -> CI/CD -> Variables
	- Create Variables:
	    
        SERVER_IP       = digitalocean server IP
	    
        SERVER_USER     = server user with sudo permissions
	    
        SSH_PRIVATE_KEY = private key to access the server (added in server's authorized keys)


- Create runner for repo
    https://gitlab.com/help/ci/quick_start/README#configuring-a-runner
- Create .gitlab-ci.yml file and place in local repo